# Frankenbulma

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

A Bulma Material Design Experiment.

> Pure HTML5 + Bulma CSS + Material Design + Custom SASS

## Experimental

This repository might change from time to time.

## To do

Porting some materialize javascript.

-- -- --

## Links

### Frankenbulma

This repository:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md]

### Comparation

Comparation with other guidance:

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

* [Semantic UI Step by Step Repository][tutorial-semantic-ui]

### Jekyll Step By Step

Using Bulma Material Design in SSG:

* [Jekyll Step by Step Repository][tutorial-jekyll]

[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/
[tutorial-semantic-ui]: https://gitlab.com/epsi-rns/tutor-html-semantic-ui/

-- -- --

Material Design start from step 04.
Step 01 until step 03 contain Bulma preparation.

## Step 01

> Adding Bulma CSS

* Without Bulma

* Using CDN

* Using Local

![screenshot][screenshot-01]

## Step 02

> Using Bulma CSS: Navigation Bar

* Simple

* Full Featured

* With jQuery

* With Vue JS

* Adding Font Awesome

![screenshot][screenshot-02]

## Step 03

> Custom SASS

* Custom maximum width class

* Responsive gap using custom sass

![screenshot][screenshot-03]

## Step 04

> Material Design

* Material Color Demo

* Spacing Helper Demo

![screenshot][screenshot-04]

## Step 05

> HTML Box using Material Design

* Main Blog and Aside Widget

![screenshot][screenshot-05]

## Step 06

> Finishing

* Blog Post Example

![screenshot][screenshot-06]

-- -- --

| Disclaimer | Use it at your own risk |
| ---------- | ----------------------- |

[screenshot-01]:         https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-01/html-bulma-md-preview.png
[screenshot-02]:         https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-02/html-bulma-md-preview.png
[screenshot-03]:         https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-03/html-bulma-md-preview.png
[screenshot-04]:         https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-04/html-bulma-md-preview.png
[screenshot-05]:         https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-05/html-bulma-md-preview.png
[screenshot-06]:         https://gitlab.com/epsi-rns/tutor-html-bulma-md/raw/master/step-06/html-bulma-md-preview.png
