// http://belowthebenthic.com/bulma-burger/

document.addEventListener("DOMContentLoaded", function(event) { 
  console.log('Document is Ready.');

  new Vue({
    el: '#navbar-vue-app',
    data: {
      isOpen: false
    },
    methods: {
      toggleOpen: function (event) {
        this.isOpen = !this.isOpen;
        console.log('Toggle is-active class in navbar burger menu.');
      }
    }
  });
});


