document.addEventListener("DOMContentLoaded", function(event) { 
  // Check for click events on the navbar burger icon
  var navbarBurgers = document.getElementsByClassName("navbar-burger");
  var navbarMenus   = document.getElementsByClassName("navbar-menu");
  
  navbarBurgers[0].onclick = function() {
    navbarBurgers[0].classList.toggle("is-active");
    navbarMenus[0].classList.toggle("is-active");
    console.log('Toggle is-active class in navbar burger menu');
  } 
});
